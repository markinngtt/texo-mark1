package com.gatti.texomark1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gatti.testetexo.TexoMark1Application;
import com.gatti.testetexo.business.Premio;
import com.gatti.testetexo.domain.Filme;
import com.gatti.testetexo.repository.FilmeRepository;
import com.gatti.testetexo.repository.WinnerMaiorIntervalRepository;
import com.gatti.testetexo.repository.WinnerMenorIntervalRepository;
import com.gatti.testetexo.response.Ganhadores;
import com.gatti.testetexo.response.MaiorInterval;
import com.gatti.testetexo.response.MenorInterval;
import com.gatti.testetexo.rest.Consulta;

@SpringBootTest(classes = TexoMark1Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Consulta.class, Premio.class, Filme.class
	})
class TexoMark1ApplicationTests {
	
    @MockBean
    private Consulta consultaRest;
    
    @MockBean
    private FilmeRepository filmeRepository;
    
    @MockBean
    private WinnerMenorIntervalRepository menorIntervalo;
    
    @MockBean
    private WinnerMaiorIntervalRepository maiorIntervalo;
    
    @MockBean
    private Premio premio;
    
	@Test
	void contextLoads() {
	}
	
//	@Test
//	void testaConsultaGanhadores() throws Exception {
//		
//		List<Filme> filmes = new ArrayList<Filme>();
//		Filme filme = new Filme();
//		filme.setId(1);
//		filme.setNomeArquivo("testeIntegracao.csv");
//		filme.setProducers("testeIntegracao");
//		filme.setStudios("testeIntegracao");
//		filme.setTitle("testeIntegracao");
//		filme.setWinner("yes");
//		
//		filmes.add(filme);		
//		Filme filme2 = new Filme();
//		
//		filme2.setId(2);
//		filme2.setNomeArquivo("testeIntegracao.csv");
//		filme2.setProducers("testeIntegracao");
//		filme2.setStudios("testeIntegracao");
//		filme2.setTitle("testeIntegracao");
//		filme2.setWinner("yes");		
//		filme2.setYear(1991);
//
//		filmes.add(filme2);		
//		
//		Filme filme3 = new Filme();
//		filme3.setId(3);
//		filme3.setNomeArquivo("testeIntegracao.csv");
//		filme3.setProducers("testeIntegracao");
//		filme3.setStudios("testeIntegracao");
//		filme3.setTitle("testeIntegracao");
//		filme3.setWinner("yes");		
//		filme3.setYear(2000);
//		filmes.add(filme3);
//		
//		filmeRepository.saveAll(filmes);
//		
//		premio.ganhadoresPremios();
//		
//		List<MenorInterval> menor = new ArrayList<MenorInterval>();
//		menor = menorIntervalo.buscaTodos();
//		
//		List<MaiorInterval> maior = new ArrayList<MaiorInterval>();
//		maior = maiorIntervalo.buscaTodos();
//		
//		Ganhadores ganhadores = new Ganhadores();
//		ganhadores.setMin(menor);
//		ganhadores.setMax(maior);
//		
//		assertEquals(ganhadores.getMin().size(), 1);
//		assertEquals(ganhadores.getMax().size(), 1);
//		
//		
//	}

}
