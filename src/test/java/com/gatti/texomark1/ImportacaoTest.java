package com.gatti.texomark1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.gatti.testetexo.repository.FilmeRepository;
//import com.gatti.testetexo.rest.ImportaCsv;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.gatti.testetexo.domain.Filme;
import com.gatti.testetexo.repository.FilmeRepository;
import com.gatti.testetexo.rest.ImportaCsv;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImportaCsv.class})
public class ImportacaoTest {
	
	//URL base para acesso desse controlador
    private final String BASE_URL = "/api/csv/importar";

    //Instância do ObjectMapper para trabalhar com JSON
    private ObjectMapper objectMapper;

    //Controlador REST tratado por meio de injeção de dependências
    @Autowired
    private ImportaCsv csv;

    //Instância do MockMVC
    private MockMvc mockMvc;

    //Instância do mock repository
    @MockBean
    private FilmeRepository filmeRepository;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders
                .standaloneSetup(csv)
                .build();
    }
    
    @Test
    void testaImportacao() throws Exception {
    	String caminho = "/texo-mark1/src/main/resources/arquivo/movielist.csv";
    	String possuiCab = "S";
    	List<Filme> filmes = new ArrayList<Filme>();
    	
    	mockMvc.perform(post(BASE_URL+"/"+caminho+"/"+possuiCab)
    		   .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
    	       .andExpect(status().isCreated());
    	//when(mockRepository.save(any(Produto.class))).thenReturn(produto);
    	//when(filmeRepository.findAll().thenReturn(filmes));
    	assertEquals(filmeRepository.findAll().size(), 2);
    	
    	
    }
	
}
