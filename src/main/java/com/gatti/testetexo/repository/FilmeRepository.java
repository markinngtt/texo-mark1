package com.gatti.testetexo.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatti.testetexo.domain.Filme;

@Repository
public interface FilmeRepository extends JpaRepository<Filme, Long>{

	@Query(value = "select x.producers, "
			+ "        x.followingwin - x.previouswin intervalo, "
			+ "        x.previouswin, "
			+ "        x.followingwin "
			+ "   from (select f.producers, "
			+ "                (select min(a.year) min_year "
			+ "                   from Filme a "
			+ "                  where a.producers like '%' || f.producers || '%' "
			+ "                    and a.winner = 'yes') previouswin, "
			+ "                (select max(a.year) max_year "
			+ "                   from Filme a "
			+ "                  where a.producers like '%' || f.producers || '%' "
			+ "                    and a.winner = 'yes') followingWin "
			+ "           from Filme f "
			+ "          where f.winner = 'yes') x "
			+ "  where x.previouswin <> x.followingwin "
			+ "  group by x.producers, x.previouswin, x.followingwin "
			+ "  order by x.followingwin - x.previouswin ", nativeQuery = true)	
	public ArrayList<String> premiados();
	
	
	@Query(value = "select f.year, f.producers "
			+ "      from filme f, "
			+ "           (select min(a.year) min_year, max(a.year) max_year, a.producers "
			+ "              from filme a "
			+ "             where a.winner = 'yes' "
			+ "             group by a.producers) min_max "
			+ "     where f.winner = 'yes' "
			+ "       and f.year between min_max.min_year and min_max.max_year "
			+ "       and f.producers like '%' || min_max.producers || '%' "
			+ "     group by f.year, f.producers, min_max.producers "
			+ "     order by min_max.producers, f.year ", nativeQuery = true)
	public ArrayList<String> ganhadores(); 
	
	@Query(value = "select min(f.year) "
			+ "      from filme f "
			+ "     where f.producers like '%' || :producer || '%' "
			+ "       and f.year <> :year "
			+ "       and f.winner = 'yes' ", nativeQuery = true)
	public Integer obtemMenorProximoAno(@Param("producer") String producer, @Param("year") Integer year);
	
}
