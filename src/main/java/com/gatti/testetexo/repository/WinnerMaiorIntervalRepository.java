package com.gatti.testetexo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gatti.testetexo.domain.WinnerMaiorInterval;
import com.gatti.testetexo.response.MaiorInterval;

@Repository
public interface WinnerMaiorIntervalRepository extends JpaRepository<WinnerMaiorInterval, Long>{
	
	@Query(value = " select new com.gatti.testetexo.response.MaiorInterval(w.producers, w.intervalo, w.previouswin, w.followingwin) from WinnerMaiorInterval w order by w.intervalo asc ")
	public List<MaiorInterval> buscaTodos();
	
}
