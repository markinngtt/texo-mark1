package com.gatti.testetexo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatti.testetexo.domain.WinnerMenorInterval;
import com.gatti.testetexo.response.MenorInterval;

@Repository
public interface WinnerMenorIntervalRepository extends JpaRepository<WinnerMenorInterval, Long>{
	
	@Query(value = " select count(1) "			
			+ "      from winner_menor_interval me "
			+ "     where me.producers = :producer "
			+ "       and me.intervalo = :intervalo ", nativeQuery = true)
	public Integer existeReg(@Param("producer") String producer, @Param("intervalo") Integer intervalo);
	
	@Query(value = " select new com.gatti.testetexo.response.MenorInterval(w.producers, w.intervalo, w.previouswin, w.followingwin) from WinnerMenorInterval w order by w.intervalo asc ")
	public List<MenorInterval> buscaTodos();
}
