package com.gatti.testetexo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gatti.testetexo.domain.WinnerInterval;

@Repository
public interface WinnerIntervalRepository extends JpaRepository<WinnerInterval, Long>{

	@Query(value = "select distinct wmin.producers, "
			+ "                    wmin.intervalo, "
			+ "                    wmin.previouswin, "
			+ "                    wmin.followingwin "
			+ "      from winner_interval wmin, "
			+ "           (select a.producers, min(a.intervalo) menor "
			+ "              from winner_interval a "
			+ "             group by a.producers "
			+ "             order by a.producers ) menor_intervalo "
			+ "     where wmin.producers = menor_intervalo.producers "
			+ "       and wmin.intervalo = menor_intervalo.menor ", nativeQuery = true)	
	public List<String> menorIntervalo();
	
	@Query(value = "select distinct wmax.producers, "
			+ "                    wmax.intervalo, "
			+ "                    wmax.previouswin, "
			+ "                    wmax.followingwin "
			+ "      from winner_interval wmax, "
			+ "           (select a.producers, max(a.intervalo) maior "
			+ "              from winner_interval a "
			+ "             group by a.producers "
			+ "             order by a.producers) maior_intervalo "
			+ "     where wmax.producers = maior_intervalo.producers "
			+ "       and wmax.intervalo = maior_intervalo.maior ", nativeQuery = true)	
	public List<String> maiorIntervalo();
	
	
}
