package com.gatti.testetexo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatti.testetexo.domain.VPremiado;
import com.gatti.testetexo.response.Intervalo;

@Repository
public interface VPremiadoRepository extends JpaRepository<VPremiado, Long>{
	
	@Query(value = "Select min(v.intervalo) from VPremiado v ")
	public Integer menorIntervalo();
	
	@Query(value = "Select max(v.intervalo) from VPremiado v")
	public Integer maiorIntervalo();
	
	@Query(value = " select new com.gatti.testetexo.response.Intervalo(v.producers, v.intervalo, v.previouswin, v.followingwin) from VPremiado v where v.intervalo = :intervalo ")
	public Intervalo findByIntervalo(@Param("intervalo") Integer intervalo);
	
}
