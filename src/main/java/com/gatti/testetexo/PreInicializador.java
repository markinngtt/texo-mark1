package com.gatti.testetexo;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.gatti.testetexo.business.Csv;

@SpringBootApplication
public class PreInicializador implements CommandLineRunner{
	
	@Autowired
	private Csv csv;
	
	@Override
    public void run(String... args) throws Exception {
        main(args);
    }

    public void main(String[] args) throws Exception {
    	String caminho = "/texo-mark1/src/main/resources/arquivo/movielist.csv";
    	String possuiCab = "S";

    	try {
    	    String scannedPackage = "arquivo/*";
    	    PathMatchingResourcePatternResolver scanner = new PathMatchingResourcePatternResolver();
    	    Resource[] resources = scanner.getResources(scannedPackage);
    	    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resources[0].getInputStream()));
    	    csv.criaListaFilmes(bufferedReader, possuiCab, caminho);
            bufferedReader.close();
    	    
    	} catch (Exception e) {
    	    throw new Exception("Não foi possivel ler o caminho do arquivo: " + e.getMessage());
    	}
    }

}
