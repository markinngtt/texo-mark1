package com.gatti.testetexo;

import org.springframework.boot.SpringApplication;

public class TexoMark1Application  {
	
	public static void main(String[] args) throws Exception {
		/*
		 * Adicionado classe PreInicializador para que ao subir a aplicação o arquivo movielist.csv seja importado automaticamente.
		 */
		SpringApplication.run(PreInicializador.class, args);
		
	}	
}

