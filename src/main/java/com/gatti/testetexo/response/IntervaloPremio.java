package com.gatti.testetexo.response;

public class IntervaloPremio {
	
	Intervalo min;
	Intervalo max;
	public Intervalo getMin() {
		return min;
	}
	public void setMin(Intervalo min) {
		this.min = min;
	}
	public Intervalo getMax() {
		return max;
	}
	public void setMax(Intervalo max) {
		this.max = max;
	}
	public IntervaloPremio(Intervalo min, Intervalo max) {
		this.min = min;
		this.max = max;
	}
	public IntervaloPremio() {
	}
	
	
}
