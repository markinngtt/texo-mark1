package com.gatti.testetexo.response;

import java.util.List;

public class Ganhadores {
	
	List<MenorInterval> min;
	List<MaiorInterval> max;
	public List<MenorInterval> getMin() {
		return min;
	}
	public void setMin(List<MenorInterval> min) {
		this.min = min;
	}
	public List<MaiorInterval> getMax() {
		return max;
	}
	public void setMax(List<MaiorInterval> max) {
		this.max = max;
	}
	public Ganhadores(List<MenorInterval> min, List<MaiorInterval> max) {
		super();
		this.min = min;
		this.max = max;
	}
	public Ganhadores() {
		super();
	}
	
	
}
