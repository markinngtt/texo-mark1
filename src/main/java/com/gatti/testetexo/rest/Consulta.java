package com.gatti.testetexo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gatti.testetexo.business.ConsultaIntervaloPremio;

@RestController
@RequestMapping("/api/consulta")
public class Consulta {
	
	@Autowired
	private ConsultaIntervaloPremio consultaIntervaloPremio;
	
	@RequestMapping(value = "/consultaIntervalo", method = RequestMethod.GET)
	public ResponseEntity<Object> consultaIntervalo() throws Exception {
		
		return  ResponseEntity.status(HttpStatus.OK).body(consultaIntervaloPremio.retornaIntervaloPremio());
	}
	
	@RequestMapping(value = "/consultaGanhadores", method = RequestMethod.GET)
	public ResponseEntity<Object> consultaGanhadores() throws Exception {
		
		return  ResponseEntity.status(HttpStatus.OK).body(consultaIntervaloPremio.retornaGanhadores());
	}
	
}
