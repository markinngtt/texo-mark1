package com.gatti.testetexo.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gatti.testetexo.business.Csv;
import com.gatti.testetexo.domain.Filme;

@RestController
@RequestMapping("/api/csv")
public class ImportaCsv {

	@Autowired
	private Csv csv;
	
	@RequestMapping(value = "/importar", method = RequestMethod.POST)
	public ResponseEntity<?> importaCsv(@RequestParam (value = "caminho", required = true) String caminho, @RequestParam (value = "possuiCabecalho", required = false, defaultValue = "S") String possuiCabecalho) throws Exception {

		List<Filme> filmes = csv.importa(caminho, possuiCabecalho);
		if (!filmes.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
		return ResponseEntity.notFound().build();
	}
	
}
