package com.gatti.testetexo.business;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatti.testetexo.domain.VPremiado;
import com.gatti.testetexo.domain.WinnerInterval;
import com.gatti.testetexo.domain.WinnerMaiorInterval;
import com.gatti.testetexo.domain.WinnerMenorInterval;
import com.gatti.testetexo.repository.FilmeRepository;
import com.gatti.testetexo.repository.VPremiadoRepository;
import com.gatti.testetexo.repository.WinnerIntervalRepository;
import com.gatti.testetexo.repository.WinnerMaiorIntervalRepository;
import com.gatti.testetexo.repository.WinnerMenorIntervalRepository;

@Component
public class Premio {

	@Autowired
	private FilmeRepository filmeRepository;
	@Autowired
	private VPremiadoRepository vPremiadosRepository;
	@Autowired
	private WinnerIntervalRepository winnerIntervalRepository;
	@Autowired
	private WinnerMenorIntervalRepository winnerMenorIntervalRepository;
	@Autowired
	private WinnerMaiorIntervalRepository winnerMaiorIntervalRepository; 
	
	public void IntervaloPremios() throws Exception {
		
		String  producers;
		Integer intervalo;
		Integer previouswin;
		Integer followingwin;
		ArrayList<VPremiado> vPremiados = new ArrayList<>();
		ArrayList<String> filmes = filmeRepository.premiados();
		for (int i = 0; i < filmes.size(); i++) {
			try {
				String[] colunas = filmes.get(i).split(",", 5);
				
				producers = colunas[0];
				intervalo = Integer.parseInt(colunas[1]);
				previouswin = Integer.parseInt(colunas[2]);
				followingwin = Integer.parseInt(colunas[3]);
				
				VPremiado vPremiado = new VPremiado();
				vPremiado.setId(i);
				vPremiado.setProducers(producers);
				vPremiado.setIntervalo(intervalo);
				vPremiado.setPreviouswin(previouswin);
				vPremiado.setFollowingwin(followingwin);
				vPremiados.add(vPremiado);
			} catch (Exception ex) {
                throw new Exception("Ocorreu algum erro ao extrair as informações. Erro: "+ex);
            }
		}
		vPremiadosRepository.saveAll(vPremiados);
	}
	
	@Transactional
	public void ganhadoresPremios() throws Exception {
	    	  
		limpaTabelas();
		
		preencheTabelaGanhadores();
		
		preencheMenorGanhador();
		
		preencheMaiorGanhador();
	}

	private void preencheMaiorGanhador() {
		List<String> winnersMaiorIntervalo = winnerIntervalRepository.maiorIntervalo();
		List<WinnerMaiorInterval> listaMaiorIntervaloGanhadores = new ArrayList<WinnerMaiorInterval>();
		Integer existeReg;
		for (int i = 0; i < winnersMaiorIntervalo.size(); i++) {
			
			String[] colunas = winnersMaiorIntervalo.get(i).split(",", 4);
			
			existeReg = winnerMenorIntervalRepository.existeReg(colunas[0], Integer.parseInt(colunas[1]));
			if (existeReg == 0) {
				WinnerMaiorInterval maiorInterval = new WinnerMaiorInterval();
				maiorInterval.setId(i);
				maiorInterval.setProducers(colunas[0]);
				maiorInterval.setIntervalo(Integer.parseInt(colunas[1]));
				maiorInterval.setPreviouswin(Integer.parseInt(colunas[2]));
				maiorInterval.setFollowingwin(Integer.parseInt(colunas[3]));
				listaMaiorIntervaloGanhadores.add(maiorInterval);
			}
		}
		
		winnerMaiorIntervalRepository.saveAll(listaMaiorIntervaloGanhadores);
	}

	private void preencheMenorGanhador() {
		
		List<String> winnersMenorIntervalo = winnerIntervalRepository.menorIntervalo();
		List<WinnerMenorInterval> listaMenorIntervaloGanhadores = new ArrayList<WinnerMenorInterval>();
		
		for (int i = 0; i < winnersMenorIntervalo.size(); i++) {
			
			String[] colunas = winnersMenorIntervalo.get(i).split(",", 4);
			
			WinnerMenorInterval menorInterval = new WinnerMenorInterval();
			menorInterval.setId(i);
			menorInterval.setProducers(colunas[0]);
			menorInterval.setIntervalo(Integer.parseInt(colunas[1]));
			menorInterval.setPreviouswin(Integer.parseInt(colunas[2]));
			menorInterval.setFollowingwin(Integer.parseInt(colunas[3]));
			listaMenorIntervaloGanhadores.add(menorInterval);
		}		
		winnerMenorIntervalRepository.saveAllAndFlush(listaMenorIntervaloGanhadores);
	}

	private void preencheTabelaGanhadores() throws Exception {
		
		String sProducers = null;
		Integer nYear = null;
		Integer nMenorProximoAno;
		Integer nPreviouswin;
		Integer nFollowingwin;
		
		ArrayList<WinnerInterval> winners = new ArrayList<>();
		ArrayList<String> ganhadores = filmeRepository.ganhadores();
		for (int i = 0; i < ganhadores.size(); i++) {
		    try {
			    String[] colunas = ganhadores.get(i).split(",", 5);
				if (sProducers == null || sProducers != colunas[1]) {
					nYear	   = Integer.parseInt(colunas[0]);
					sProducers = colunas[1];
				}
				nMenorProximoAno = filmeRepository.obtemMenorProximoAno(sProducers, nYear);
				if (nMenorProximoAno != null && sProducers != null) {
					if (nMenorProximoAno > nYear) {
					    nPreviouswin  = nYear;
					    nFollowingwin = nMenorProximoAno;
					}else {
					    nPreviouswin  = nMenorProximoAno;
					    nFollowingwin = nYear;
					}
					if (nPreviouswin != nFollowingwin) {
						WinnerInterval winner = new WinnerInterval();
						winner.setId(i);
						winner.setProducers(sProducers);
						winner.setIntervalo(nFollowingwin - nPreviouswin);
						winner.setPreviouswin(nPreviouswin);
						winner.setFollowingwin(nFollowingwin);
						winners.add(winner);
					}
				} 
			}catch (Exception ex) {
			    throw new Exception("Ocorreu algum erro ao extrair as informações. Erro: "+ex);
			}
		    winnerIntervalRepository.saveAllAndFlush(winners);
		}
	}

	private void limpaTabelas() {
		winnerIntervalRepository.deleteAll();
		winnerMenorIntervalRepository.deleteAll();
		winnerMaiorIntervalRepository.deleteAll();
	}
}
	
