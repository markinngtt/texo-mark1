package com.gatti.testetexo.business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gatti.testetexo.domain.Filme;
import com.gatti.testetexo.repository.FilmeRepository;
import com.opencsv.exceptions.CsvException;

@Service
public class Csv {
	
	@Autowired
	private FilmeRepository filmeRepository;
	
	@Transactional
	public List<Filme> importa(String caminho, String possuiCabecalho) throws IOException, CsvException, Exception {
		List<Filme> filmes;
		filmeRepository.deleteAll();
		if (!validaExtensao(obtemNomeArquivo(caminho)).toUpperCase().equals("CSV")) {
			throw new Exception("O arquivo importado deve ter a extensão .csv.");
		}
        try  {
        	BufferedReader br = new BufferedReader(new FileReader(caminho));
        	filmes = salvaFilmes(criaListaFilmes(br, possuiCabecalho, caminho));
        	br.close();
        	return filmes;
        } catch (Exception e) {
        	throw new IOException("Ocorreu algum erro ao ler o arquivo. Erro: "+e.getMessage());
        }
	}
	
	@Transactional
	public List<Filme> criaListaFilmes(BufferedReader br, String possuiCabecalho, String caminho) throws CsvException, IOException{
		Integer id = 1;		
		String nomeArquivo = obtemNomeArquivo(caminho);
		String linha;
		ArrayList<Filme> filmes = new ArrayList<>();
        boolean cabecalho = (possuiCabecalho.isEmpty())?true:possuiCabecalho.equals("N")?false:true;
        while ((linha = br.readLine()) != null) {
            if (cabecalho || linha.trim().isEmpty()) {
                cabecalho = false;
                continue;
            }
            String[] colunas = linha.split(";", 5); 
            if (colunas.length != 5) {
                throw new CsvException("O arquivo CSV deve ter 5 colunas!");
            }
            try {
            	Integer year;
            	try {
            		year = Integer.parseInt(colunas[0].trim());
            	}catch (Exception e) {
            		throw new Exception("o campo Year do arquivo CSV deve ser um numero. Erro: "+e.getMessage());
				}
            	
                String title = colunas[1].trim();
                String studios = colunas[2].trim();
                String producers = colunas[3].trim();
                String winner = colunas[4].trim();

                Filme filme = new Filme(); 
                filme.setId(id++);
                filme.setYear(year);;
                filme.setTitle(title);
                filme.setStudios(studios);
                filme.setProducers(producers);
                filme.setWinner(winner);
                filme.setNomeArquivo(nomeArquivo);
                filmes.add(filme);
            } catch (Exception ex) {
                throw new CsvException("Ocorreu algum erro ao ler o arquivo. Erro: "+ex);
            }
        }
        return salvaFilmes(filmes);
	}
	
	private static String obtemNomeArquivo(String caminho) {
		
		int contador = caminho.length();
		String nomeArquivo;
	    for(int i = caminho.length()-1;i >= 0; i--){  
	        if (caminho.substring(i,i+1).equals("/") || caminho.substring(i,i+1).equals("\\") ){
	            nomeArquivo = caminho.substring(i+1,contador); 
	        	if (!nomeArquivo.isEmpty()) {
	        		System.out.println("nome: "+nomeArquivo);
	        		return nomeArquivo;
	        	}
			}
	    }
		return "";
	}
	
	private List<Filme> salvaFilmes(List<Filme> filmes){
		return filmeRepository.saveAll(filmes);
	}
	
	private String validaExtensao(String nomeArquivo) {
		
		int contador = nomeArquivo.length();
		String extensao;
	    for(int i = nomeArquivo.length()-1;i >= 0; i--){
	        if (nomeArquivo.substring(i,i+1).equals(".")){
	        	extensao = nomeArquivo.substring(i+1,contador); 
	        	if (!extensao.isEmpty()) {
	        		System.out.println("extensao: "+extensao);
	        		return extensao;
	        	}
			}
	    }
		return "";
	}
	
}
