package com.gatti.testetexo.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatti.testetexo.repository.VPremiadoRepository;
import com.gatti.testetexo.repository.WinnerMaiorIntervalRepository;
import com.gatti.testetexo.repository.WinnerMenorIntervalRepository;
import com.gatti.testetexo.response.Ganhadores;
import com.gatti.testetexo.response.Intervalo;
import com.gatti.testetexo.response.IntervaloPremio;
import com.gatti.testetexo.response.MaiorInterval;
import com.gatti.testetexo.response.MenorInterval;

@Component
public class ConsultaIntervaloPremio {
	
	@Autowired
	VPremiadoRepository vPremiadoRepository;
	@Autowired
	WinnerMenorIntervalRepository menorPremio;
	@Autowired
	WinnerMaiorIntervalRepository maiorPremio;
	@Autowired
	Premio premio;
	public IntervaloPremio retornaIntervaloPremio() throws Exception {
		
		IntervaloPremio intervaloPremio = new IntervaloPremio();
		
		premio.IntervaloPremios();
		
		Intervalo menorIntervalo = vPremiadoRepository.findByIntervalo(vPremiadoRepository.menorIntervalo());
		
		Intervalo maiorIntervalo = vPremiadoRepository.findByIntervalo(vPremiadoRepository.maiorIntervalo());
		
		intervaloPremio.setMin(menorIntervalo);
		intervaloPremio.setMax(maiorIntervalo);
		
		return intervaloPremio;
		
	}
	
	public Ganhadores retornaGanhadores() throws Exception {
		Ganhadores ganhadores = new Ganhadores();
		
		premio.ganhadoresPremios();
		List<MenorInterval> menorIntervalo = menorPremio.buscaTodos();
		List<MaiorInterval> maiorIntervalo = maiorPremio.buscaTodos();
		ganhadores.setMin(menorIntervalo);
		ganhadores.setMax(maiorIntervalo);
		
		return ganhadores;
		
	}
}
