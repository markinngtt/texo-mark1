package com.gatti.testetexo.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "winner_maior_interval")
public class WinnerMaiorInterval {
	
	@Id
	Integer id;
	String producers;
	Integer intervalo;
	Integer previouswin;
	Integer followingwin;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProducers() {
		return producers;
	}
	public void setProducers(String producers) {
		this.producers = producers;
	}
	public Integer getIntervalo() {
		return intervalo;
	}
	public void setIntervalo(Integer intervalo) {
		this.intervalo = intervalo;
	}
	public Integer getPreviouswin() {
		return previouswin;
	}
	public void setPreviouswin(Integer previouswin) {
		this.previouswin = previouswin;
	}
	public Integer getFollowingwin() {
		return followingwin;
	}
	public void setFollowingwin(Integer followingwin) {
		this.followingwin = followingwin;
	}
	public WinnerMaiorInterval(Integer id, String producers, Integer intervalo, Integer previouswin, Integer followingwin) {
		super();
		this.id = id;
		this.producers = producers;
		this.intervalo = intervalo;
		this.previouswin = previouswin;
		this.followingwin = followingwin;
	}
	public WinnerMaiorInterval() {
		super();
	}	
	
}
