# README #
Para executar o projeto, baixe o arquivo texo-mark1.jar e execute java -jar texo-mark1.jar.
Arquivo foi substituido com a correção.
#
Ao iniciar o sistema, o arquivo movielist.csv sera carregado automaticamente.
#
Os testes devem ser realizados via postman.
#
Não consegui implementar os testes de integração.
#
Criei um nova API pra retornar o produtores com menor/maior intervalo de premios (API -> consultaGanhadores), diferente da antiga (API -> consultaIntervalo), que retornava apenas o menor/maior entre todos do arquivo.
Não removi a API antiga(consultaIntervalo), crie uma nova (consultaGanhadores).
## 1. Apis disponiveis
/api/csv/importar
#
/api/consulta/consultaIntervalo
#
/api/consulta/consultaGanhadores
### 1.1 Importar (/api/csv/importar)
Api importar é do tipo POST e possui dois parametros:
#
  caminho -> deve ser informado o caminho do arquivo .CSV que o sistema irá importar. Deve ser informar o caminho utilizando / para separar as pastas
#    
	exemplo: C:/Users/Documents/movielist.csv	
# 
 possuiCabecalho -> deve ser S/N caso não informado o valor padrão é S
#
Exemplo da chamada via postman, API do tipo POST ->
#
  http://localhost:8080/api/csv/importar?caminho=C:/Users/Documents/movielist.csv&possuiCabecalho=S
#  
### 1.2 Consulta Intervalo (/api/consulta/consultaIntervalo)  
#
Api consulta intervalo é do tipo Get e não possui parametros.
#
Exemplo da chamada via postman, API do tipo GET ->
#
  http://localhost:8080/api/consulta/consultaIntervalo  
  
### 1.3 Consulta Ganhadores (/api/consulta/consultaIntervalo)  
#
Api consulta consultaGanhadores é do tipo Get e não possui parametros.
#
Exemplo da chamada via postman, API do tipo GET ->
#
  http://localhost:8080/api/consulta/consultaGanhadores